;;; init.el --- Summary: Foxmean's init.el file for termux
;;; Commentary:
;; I put these Emacs Lisp documentation strings in here (and below) because I really,
;; really, want Flycheck to like me.  I crave its approval.
;;; Code:
(setq user-full-name "Pathompong Kwangtong")
(setq user-mail-address "foxmean@protonmail.com")

(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)

(package-initialize)

;; use-package
;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
(package-refresh-contents)
(package-install 'use-package))

;;; packages
;; org-mode
(use-package org
      :ensure t
      :pin org)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c r") 'org-capture)
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c L") 'org-insert-link-global)
(global-set-key (kbd "C-c O") 'org-open-at-point-global)
(global-set-key (kbd "<f9> <f9>") 'org-agenda-list)
(global-set-key (kbd "<f9> <f8>") (lambda () (interactive) (org-capture nil "r")))
(setq org-capture-templates '(
  ("n" "Note" entry (file+headline "~/journals/organizer.org" "Note")
    "**  %? :note:\n   <%<%Y-%m-%d %a>>\n"
    :empty-lines 1)
  ("t" "Task, scheduled (not yet categorize)" entry (file+headline "~/journals/organizer.org" "Task(s): not yet categorized")
    "** TODO [#A] %?\n   DEADLINE: <%<%Y-%m-%d %a>>\n"
    :empty-lines 1)))


;; ledger-mode
(use-package ledger-mode
  :ensure t
  :init
  :config
  :mode "\\.dat\\'")
(setq ledger-clear-whole-transactions 1)


;; company-mode
(use-package company
:ensure t
:config
(setq company-idle-delay 0)
(setq company-minimum-prefix-length 3)

(global-company-mode t)
)

;; flycheck
(use-package flycheck
  :ensure t
  :init
  (global-flycheck-mode t))

;; git
(use-package magit
    :ensure t
    :init
    (progn
      (bind-key "C-x g" 'magit-status)
      (bind-key "C-x M-g" 'magit-dispatch)
      (global-magit-file-mode)
    ))


;;; customize UI
(setq inhibit-startup-message t)
(global-prettify-symbols-mode 1) ; display “lambda” as “λ”
(defalias 'yes-or-no-p 'y-or-n-p)
; highlight matching parenthesis
(show-paren-mode 1)
(global-visual-line-mode 1)

;;; Customize-variables
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ledger-reports
   (quote
    (("bal" "%(binary) -f %(ledger-file) bal")
     ("reg" "%(binary) -f %(ledger-file) reg")
     ("payee" "%(binary) -f %(ledger-file) reg @%(payee)")
     ("account" "%(binary) -f %(ledger-file) reg %(account)"))))
 '(org-agenda-files (quote ("~/journals/organizer.org")))
 '(org-todo-keywords
   (quote
    ((sequence "TODO(t)" "WAITING(w)" "SOMEDAY(s)" "DONE(d)"))))
 '(package-selected-packages
   (quote
    (avy zetteldeft deft magit flycheck company ledger-mode use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; deft
(use-package deft
  :ensure t
  :custom
    (deft-extensions '("org" "md" "txt"))
    (deft-directory "~/notes")
    (deft-use-filename-as-title t))

;; org-roam
(use-package org-roam
      :ensure t
      :hook
      (after-init . org-roam-mode)
      :custom
      (org-roam-directory "~/notes")
      :bind (:map org-roam-mode-map
              (("C-c n l" . org-roam)
               ("C-c n f" . org-roam-find-file)
               ("C-c n g" . org-roam-show-graph))
              :map org-mode-map
              (("C-c n i" . org-roam-insert))))

;;; init.el ends here
